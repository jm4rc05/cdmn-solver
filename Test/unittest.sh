# NORMAL IDP CHECKS BASED ON XLSX #################################################################
sheets=("Map_Coloring" "Monkey_Business" "Reindeer_Ordering" "Hamburger_Challenge" 
        "Balanced_Assignment Balanced_Assignment_Datatable"
        "Duplicate_Product_Lines" "Customer_Greeting" "Crack_the_Code"
        "Family_Riddle" "Map_Coloring_Violations" "Zoo_Buses_and_Kids" "Online_Dating" "Collection_of_Cars"
        "Who_Killed_Agatha" "Vacation_Days" "Vacation_Days_Advanced" "Change_Making" "EquationHaiku_alt"
        "Nim_Rules" "Calculator")
data="Examples/DMChallenges.xlsx"
tempfile="temp.txt"
idpfile="idptemp.txt"
# idppath="~/idp/usr/local/bin/idp"
idppath="~/idp/bin/idp"
# idppath="idp"

for sheet in "${sheets[@]}";
do
    echo $data "$sheet"
    python3 -O solver.py $data -n $sheet -o "./temp.idp"> $tempfile 2> $tempfile --idp $idppath
    if grep -q "Error" $tempfile; then
        echo "Probleem!"
        exit 1
    fi
    echo ".idp file made!"
    if grep -q "Unsatisfiable"  $idpfile; then
        echo "Unsatisfiable!"
        exit 1
    fi
    echo "Satisfiable!"
done

sheets=("Sheet1")
data="Examples/Extended_Tests.xlsx"
for sheet in ${sheets[*]};
do
    echo $data "$sheet"
    python3 -O solver.py $data -n "$sheet" -o "./temp.idp"> $tempfile 2> $tempfile --idp $idppath
    if grep -q "Error" $tempfile; then
        echo "Probleem!"
        exit 1
    fi
    echo ".idp file made!"
    if grep -q "Unsatisfiable"  $idpfile; then
        echo "Unsatisfiable!"
        exit 1
    fi
    echo "Satisfiable!"
done


# NORMAL IDP CHECKS BASED ON XML ##################################################################
xmls=("A.1.dmn" "A.2.dmn" "food.dmn" "order_discount.dmn" "vacation_days.dmn" "AnimalLicensePrice.dmn"
      "AnimalLicensePrice_large.dmn" "harbour.dmn" "LogicCircuit.dmn" "PostBureau.dmn" "SimpleBool.dmn" "salary.dmn"
      "advertisement.dmn" "terrace.dmn" "mini_BMILevel.dmn") # "food_predefined.dmn")

for xml in ${xmls[*]};
do
    echo $xml
    # Try the conversion to the IDP-Z3 Interactive Consultant.
    python3 -O solver.py "./Examples/XML/$xml" -o test.idp --interactive-consultant-idp-z3 > $tempfile 2>&1
    if grep -q "Error" $tempfile; then
        echo "Probleem!"
        exit 1
    fi
    echo "Convertable to IDPz3!"
done

# IDPZ3 CHECKS BASED ON XLSX AND XML ##############################################################
sheets=("Map_Coloring" "Monkey_Business" #  "Crack_the_Code"
        #"Doctor_Planning"
        "Duplicate_Product_Lines" "EquationHaiku" "EquationHaiku_alt"
        "Nim_Rules" #"Online_Dating"
        "Vacation_Days_alt"
        "Vacation_Days") # "Who_Killed_Agatha") 
data="Examples/DMChallenges.xlsx"
for sheet in ${sheets[*]};  # Generate idpz3 of every working sheet.
do
    echo "z3:" $data "$sheet"
    python3 -O solver.py $data -n "$sheet" -o "./$sheet.idp" --idp-z3 --interactive-consultant-idp-z3 > $tempfile 2>&1
    if grep -q "Error" $tempfile; then
        echo "Probleem!"
        cat $tempfile
        exit 1
    fi
done

for xml in ${xmls[*]};  # Generate idpz3 of every xml.
do
    echo "z3:" $xml
    python3 -O solver.py "./Examples/XML/$xml" -o "./$xml.idp" --idp-z3 > $tempfile 2>&1
    if grep -q "Error" $tempfile; then
        echo "Probleem!"
        cat $tempfile
        exit 1
    fi
done

overlap_error=("food_duplicate.dmn" "food_duplicate2.dmn" "food_duplicate3.dmn")
output_error=("dish-decision" "dish-decision" "dish-decision")

for ((i=0;i<${#overlap_error[@]};++i));  # Generate idpz3 of every error checking xml
do
    xml="${overlap_error[i]}"
    overlap="${output_error[i]}"
    echo "z3:" $xml
    python3 -O solver.py "./Examples/XML/$xml" -o "./$xml.idp" --idp-z3 --errorcheck-overlap $overlap > $tempfile 2>&1
    if grep -q "Error" $tempfile; then
        echo "Probleem!"
        exit 1
    fi
done

