# XML examples

These are some examples found on the internet, and some made by myself.
I in no way own these examples.

| Example  | Origin  |
|---|---|
| food & food2  | Camunda examples  |
| A.1, A.2, order\_discount | [tck example](https://github.com/dmn-tck/tck/tree/master/TestExamples)  |
| Vacation Days | Based on [DMCommunity Challenge](https://dmcommunity.org/challenge/challenge-jan-2016/)  |
| Dog Licence pricing | [Example by StephenOTT](https://github.com/StephenOTT/Gov-DMN-Dog-License-Pricing-Example)|
| Post Bureau | [Sparxsystems](https://sparxsystems.com/enterprise_architect_user_guide/15.1/model_domains/dmn_decisiontable_validation.html) |
| Simple bool | Just simple boolean stuff. |
| Logical Circuit | A commmon example found in papers, encoded in DMN by me. |
| Advertisement | ?? |
| Prepare Terrace | ?? |
| Check Order | ?? |
| Leave Application | [Camunda](https://docs.camunda.org/manual/7.12/reference/dmn11/decision-table/hit-policy/) |
| Salary | ?? |
| Application Risk | ?? |
| Car Insurance | ?? |
